import React from "react";
import { createDrawerNavigator } from "react-navigation-drawer";


//Import Custom Sidebar
import CustomSidebarMenu from './CustomSidebarMenu';

import Dashboard from "./Dashboard";
import Campaign from "./Campaign";
import * as theme from "../constants/theme";


const DrawerNavigatorConfig = {
  
};

export default createDrawerNavigator(
  {
    Dashboard,
    Campaign,
  },
  {
    contentComponent: CustomSidebarMenu,
  }, DrawerNavigatorConfig);
