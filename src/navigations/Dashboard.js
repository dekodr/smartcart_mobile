import React from "react";
import { createStackNavigator } from "react-navigation-stack";

import Dashboard from "_screens/dashboard/index";
import ShareSmartcart from "_screens/dashboard/ShareSmartcart";
import WatchVideo from "_screens/dashboard/WatchVideo";

export default createStackNavigator(
  {
    Dashboard,
    ShareSmartcart,
    WatchVideo
  }
);
