import React, { Component } from 'react';
import { 
    TouchableOpacity, 
    TouchableWithoutFeedback,
	Image, 
	SafeAreaView, 
	ScrollView, 
	StyleSheet,
	Dimensions, 
	View
} from 'react-native';
import { Block, Button, Text, CustomIcon} from '_atoms';
import Icon from 'react-native-vector-icons/FontAwesome';
import * as theme from '../../constants/theme';
import Video from 'react-native-video';

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

class WatchVideo extends Component {
	
	static navigationOptions = ({ navigation }) => ({
		headerTransparent: true,
		headerStyle: {
			borderBottomWidth: 0
		},
		headerLeftContainerStyle: {
			paddingLeft: 24
		},
		headerRightContainerStyle: {
			paddingRight: 24
		},
		headerTitleContainerStyle: {
			paddingTop: 15
		},
		headerLeft: (
			<TouchableOpacity onPress={() => navigation.goBack()}>
				<CustomIcon back />
			</TouchableOpacity>
		)
	});
	
	constructor(props) {
        super(props);

        this.state = {
            repeat: false,
            rate: 1,
            volume: 1,
            muted: false,
            paused: true,
            resizeMode: 'contain',
            hideControls: false
        };
    }
	render() {
		return (
			<SafeAreaView>
				<ScrollView>
                    <View  style={styles.body}>
                        <View style={styles.accent1}>
                            <Image
                                source={require('../../assets/images/Base/accent-top.png')}
                                style={styles.accentImage}
                            />
                        </View>
                        <View style={styles.margin}>
                            <View style={styles.titleHeader}>
                                <Text h2 bold>Watch Video</Text>
                            </View>
                            <Block center style={{paddingTop:40}}>

                                <TouchableWithoutFeedback
                                onPress={() => this.setState({paused: !this.state.paused})}>
                                    <Video 
                                    ref={(ref) => {
                                        this.player = ref
                                    }}           source={require('../../assets/images/Base/video.mp4')}   
                                    volume={this.state.volume}
                                    muted={this.state.muted}
                                    resizeMode={this.state.resizeMode}
                                    paused={this.state.paused}
                                    hideControls={this.state.hideControls}
                                    style={styles.backgroundVideo} />
                                </TouchableWithoutFeedback>    

                                <Text light h4 style={{paddingVertical:20}}>Watch videos now and get 30 points!</Text>
                                <Button full>
                                    <Text button>Play Video</Text>
                                </Button>
                            </Block>
                        </View>
                    </View>    
				</ScrollView>
			</SafeAreaView>
		)
	}
}


const styles = StyleSheet.create({
    body: { 
        minHeight: viewportHeight,
    },
    margin: {
        marginHorizontal: 24
    },
    titleHeader: {
        marginTop: 120,

    },  
	accent1: {
		height: 200,
		position: 'absolute',
		top: 0,
		alignSelf: 'flex-start'
	},
	accentImage: {
		width: viewportWidth
    },
    shareButton: {
        marginBottom: 20,
        borderRadius: 10,
        elevation: 2,
        paddingHorizontal: 20,
        paddingVertical: 25
    },
    facebook: {
        backgroundColor: "#4481BE"
    },
    twitter: {
        backgroundColor: "#60C4F8"
    },
    instagram: {
        backgroundColor: '#AF6EBA'
    },  
    checkFrame: {
        width: 80
    },
    backgroundVideo: {
        width: viewportWidth - 24,
        maxWidth: 360,
        height: 240,
        borderRadius: 16,
        overflow: 'hidden'
    }
});


export default WatchVideo;
