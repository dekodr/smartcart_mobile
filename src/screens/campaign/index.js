import React, { Component } from 'react';
import { 
	TouchableOpacity, 
	SafeAreaView, 
	ScrollView, 
	StyleSheet,
	View,
	Image
} from 'react-native';
import { Block, Card, Text, Icon, Label, HeaderSubmenu } from '_atoms';
import * as theme from '../../constants/theme';

const styles = StyleSheet.create({
	bgbody: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: theme.colors.body,
	},
	bgbodyWhite: {
		backgroundColor: theme.colors.body
	},
	skeletonWrapper: {
		marginHorizontal: 40,
		marginBottom: 20,
	},
	contentWrapper: {
		marginBottom: 80
	},
	margin: {
		marginHorizontal: 20,
		marginBottom: 10,
	},
	headerWrapper: {
		marginVertical: 30,
		marginHorizontal: 30,
	},
	headerTitle: {
		color: theme.colors.white,
		zIndex: 2,
		marginVertical: 20,
		marginHorizontal: 30
	},	
	titleCamp: {
		marginTop: 10,
		marginBottom: 5
	},
	iconCamp: {
		display: 'flex'
	},
	dateCamp: {
		display: 'flex'
	},
	standaloneRowBack: {
        alignItems: 'center',
		borderRadius: 15,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
		marginHorizontal: 20,
		marginBottom: 10,
		borderWidth: 3,
		borderColor: theme.colors.body,
		backgroundColor: '#cde1ff',
		overflow: 'hidden'
    },
    backTextEdit: {
		marginRight: -16,
		color: '#FFF',
		alignSelf: 'stretch',
		width: 90,
		alignContent: 'center',
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor:'#cde1ff',
		borderBottomRightRadius: 15,
		borderTopRightRadius: 15,
		zIndex: 2,
		paddingLeft: 15
	},
	backTextDelete: {
		color: '#FFF',
		alignSelf: 'stretch',
		width: 90,
		alignContent: 'center',
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#ffb0b6',
		zIndex: 1,
		paddingLeft: 15
    }
});

class Campaign extends Component {

	static navigationOptions = ({ navigation }) => ({
		headerStyle: {
			backgroundColor: theme.colors.primary,
			shadowColor: 'rgba(55,55,55,0)',
			elevation: 0
		},
		headerLeftContainerStyle: {
			paddingLeft: 24
		},
		headerRightContainerStyle: {
			paddingRight: 24
		},
		headerTitleContainerStyle: {
			paddingTop: 15
		},
		headerLeft: (
			<TouchableOpacity onPress={() => navigation.openDrawer()}>
				<Icon menu />
			</TouchableOpacity>
		),
		headerTitle: (
			<Block row middle></Block>
		),
		headerRight: (
			<TouchableOpacity onPress={() => navigation.navigate('insertCampaign')}>
				<Block row middle center style={{marginTop: 0 }}>
					<Text style={{color: '#fff'}}>Add New  </Text><Icon add />
				</Block>
			</TouchableOpacity>
		)
	});

	render() {

		return (
			<SafeAreaView style={styles.bgbody}>
				<ScrollView style={styles.bgbodyWhite}>
					<HeaderSubmenu title="Campaign" />
					
				</ScrollView>
			</SafeAreaView>
		)
	}
}

export default Campaign;
