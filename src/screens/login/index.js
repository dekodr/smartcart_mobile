import React, { Component } from 'react';
import { 
	StyleSheet, 
	Image, 
	KeyboardAvoidingView, 
	View,
	Dimensions,
	Keyboard,
	SafeAreaView,
	TouchableWithoutFeedback 
} from 'react-native';

import { Button, Block, Text, Input } from '_atoms';
import * as theme from '../../constants/theme';
import { ScrollView } from 'react-native-gesture-handler';
  

const { height: viewportHeight, width: viewportWidth } = Dimensions.get('window');

class Login extends Component {

	render() {
		const { navigation } = this.props;
		
		return (
			<>
			<SafeAreaView style={styles.keyboardAvoid}>
				<ScrollView>
					
					<TouchableWithoutFeedback onPress={Keyboard.dismiss}>
						<Block>
							<View style={styles.accent1}>
								<Image
									source={require('../../assets/images/Base/accent-top.png')}
									style={styles.accentImage}
								/>
							</View>
							<Block style={styles.topHeight}></Block>
							<Block >
								<Block style={styles.loginHeader}>
									<Image
										source={require('../../assets/images/Base/Logo-2.png')}
										style={{ height: 40, width: 180, marginBottom: 10}}
									/>
									<Text h2 style={styles.loginHeaderText}>Welcome</Text>
									<Text captionDisabled>PLEASE LOGIN FIRST</Text>
								</Block>	
								
								<Block center style={{ marginTop: 10 }}>
									<Input
										full
										placeholder="Username"
										style={{ marginBottom: 10 }}
									/>
									<Input
										placeholder="Password"
										full
										password
										style={{ marginBottom: 25 }}
									/>

									<Button
										style={{ marginTop: 30 }}
										onPress={() => navigation.navigate('Dashboard')}>
										<Text button>Sign in</Text>
									</Button>
									<Button
										secondary
										style={{ marginTop: 20 }}
										onPress={() => navigation.navigate('Dashboard')}>
										<Text color="black" bold>Register</Text>
									</Button>
								</Block>
							</Block>
						</Block>
					</TouchableWithoutFeedback>
				</ScrollView>
			</SafeAreaView>	
			</>
		)
	}
}

const styles = StyleSheet.create({
	keyboardAvoid: {
		flex: 1,
		backgroundColor: theme.colors.body
	},	
	accent1: {
		height: 200,
		position: 'absolute',
		top: 0,
		alignSelf: 'flex-start'
	},
	accentImage: {
		width: viewportWidth
	},
	topHeight: {
		height: viewportHeight / 7
	},	
	loginHeader: {
		marginLeft: 30
	},	
	loginHeaderText: {
		marginTop: 50,
		marginBottom: 10
	},
	headerBG: {
		position: "absolute",
		bottom: 20,
		right: 0,
		zIndex: 0,
		width: 300
	},
	containerActivity: {
		position: 'absolute',
		left: 0,
		right: 0,
		top: 0,
		bottom: 0,
		alignItems: 'center',
		zIndex: 2,
		flex: 1,
		justifyContent: 'center',
		flexDirection: 'column'
	},
	horizontalActivity: {
		paddingTop: 30,
		flex: 0.1,
		backgroundColor: `rgba(0,0,0,0.6)`
	},
	textActivity: {
		color: '#fff',
		paddingTop: 20
	}
});

export default Login;