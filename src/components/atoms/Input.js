import React, { Component } from 'react'
import { StyleSheet, View, TextInput, Dimensions } from 'react-native'
import Text from './Text';
import * as theme from '../../constants/theme';

const { width } = Dimensions.get("window");

export default class Input extends Component {
  
  render() {
    const { label, rightLabel, height, full, email, phone, number, password, style, ...props } = this.props;
    const inputStyles = [
      styles.input,
      full && styles.full,
      height && styles.height,
      style,
    ];

    const inputType = email
      ? 'email-address' : number
      ? 'numeric' : phone
      ? 'phone-pad' : 'default';

    return (
      <View>
        <View style={styles.labelContainer}>
          <Text style={styles.label}>
            {label}
          </Text>
          {rightLabel}
        </View>
        <TextInput
          style={inputStyles}
          secureTextEntry={password}
          autoCapitalize="none"
          autoCorrect={false}
          keyboardType={inputType}
          {...props}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  input: {
    backgroundColor: theme.colors.input,
    borderWidth: 0,
    borderRadius: 8,
    fontSize: theme.sizes.font,
    color: theme.colors.black,
    height: 50,
    paddingVertical: 12,
    paddingHorizontal: 16,
    shadowColor: theme.colors.red,
		shadowOpacity: 1,
		shadowRadius: 8,
		shadowOffset: { width: 2, height: 8 },
    fontFamily: 'CeraPro-Regular',
		elevation: 2
  },
  label: {
    color: theme.colors.gray3,
    fontFamily: 'CeraPro-Regular',
  },
  labelContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 8,
    marginLeft: 15,
    
  },
  full: {
    width: width - 50,
  },
  height: {
    height: 150,
    textAlignVertical: 'top'
  }
});
