import React, { Component } from 'react'
import { StyleSheet, View, Image } from 'react-native'
import * as theme from '../../constants/theme';

import Block from './Block';
import Text from './Text';

export default class HeaderSubmenu extends Component {

	renderHeader = () => {
		const { title } = this.props;
		if (!title) return null;

		return (
			<View style={styles.BgWrapper}>
				<Text h3 style={styles.headerTitle}>{title}</Text>
				<Image
					source={require('../../assets/images/header-background.png')}
					style={styles.headerBG}
				/>
				<View style={styles.roundedFooter} />
			</View>
		)
	}

	render() {
		const { children, ...props } = this.props;

		return (
			<Block {...props}>
				{this.renderHeader()}
				{children}
			</Block>
		)
	}
}
const styles = StyleSheet.create({
	headerTitle: {
		color: theme.colors.white,
		zIndex: 2,
		marginVertical: 20,
		marginHorizontal: 30

	},	
	BgWrapper: {
		height: 130,
        flexDirection: 'column',
        justifyContent: 'flex-end',
		backgroundColor: '#10A98F'
	},
	roundedFooter: {
		backgroundColor: theme.colors.body,
		height: 32,
		borderTopRightRadius: 32,
		borderTopLeftRadius: 32,
		shadowRadius: 12,
		zIndex: 2
	},
	headerBG: {
		position: "absolute",
		bottom: 0,
		right: 0,
		zIndex: 0,
		width: 250,
		opacity: 0.5
	}
});
