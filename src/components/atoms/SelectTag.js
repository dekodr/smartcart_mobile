import React, { Component } from 'react'
import { TouchableOpacity, StyleSheet, View, Dimensions } from 'react-native'
import Text from './Text';
import Label from './Label';
import CustomIcon from './CustomIcon';
import * as theme from '../../constants/theme';

export default class Datepicker extends Component {

	render(){

		const { label, id, ...props } = this.props;
		const inputStyles = [
			styles.tagContainer,
		];

		return (
			<View style={styles.tagContainer}>
				{/* <Label white style={styles.dot} /> */}
				<Text style={styles.label}>
					{label}
				</Text>
				<TouchableOpacity onPress={()=>this.props.onRemove(id)}><CustomIcon cancelW /></TouchableOpacity>
			</View>	
		)
	}
}

const styles = StyleSheet.create({
	tagContainer: {
		flex: 1,
		backgroundColor: theme.colors.gray,
		borderRadius: 20,
		padding: 6,
		marginBottom: 5,
		marginRight: 5,
		paddingLeft: 16,
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	dot: {
		marginRight: 10
	},
	label: {
		color: theme.colors.black
	}
});